from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

app_name = 'accounts'

urlpatterns = [
    path('login/', views.SubLoginView.as_view(template_name='accounts/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(
        next_page='accounts:login'), name='logout'),
    path('signup/', views.SignupCreateView.as_view(), name='signup'),
    path('profile/', views.profile_view, name='profile'),
]
