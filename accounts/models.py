from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    biography = models.CharField(max_length=200, blank=True)
    gender = models.CharField(max_length=20, blank=True)
    picture = models.ImageField(upload_to='users/profile/', blank=True)
    website = models.URLField(max_length=100, blank=True)
