from django.contrib.auth import views, forms
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import SignupForm

# Create your views here.
class SubLoginView(views.LoginView):
    form = forms.AuthenticationForm


def profile_view(request):
    return render(request, 'accounts/profile.html', {'user': request.user})


class SignupCreateView(CreateView):
    form_class = SignupForm
    success_url = reverse_lazy('accounts:login')
    template_name = 'accounts/signup.html'
