from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from .models import Profile

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    password = forms.PasswordInput()

class SignupForm(UserCreationForm):
    email = forms.EmailField(label='email')
    first_name = forms.CharField(label='first_name')

    class Meta:
        model = User
        fields = ('email', 'first_name', 'username', 'password1', 'password2')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            Profile.objects.create(user=user)

        return user