from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import DetailView

from .models import Post

# Create your views here.
@login_required
def index_view(request):
    posts = Post.objects.all()
    return render(request, 'posts/index.html', {
        'posts': posts,
        'me': request.user,
        })

class UserProfileView(LoginRequiredMixin, DetailView):
    template_name = 'posts/user_profile.html'
    model = User
    slug_url_kwarg = 'username'
    slug_field = 'username'
