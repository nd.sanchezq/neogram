from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    description = models.CharField(max_length=255)
    picture = models.ImageField(upload_to='posts/pics/')

    created = models.DateTimeField(auto_now_add=True)
    Modified = models.DateTimeField(auto_now=True)
