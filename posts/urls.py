from django.urls import path
from . import views 


app_name = 'posts'

urlpatterns = [
    path('', views.index_view, name='index'),
    path('<str:username>/', views.UserProfileView.as_view(), name='profile'),
]
